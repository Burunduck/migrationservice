using System;
using Npgsql;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using MigrationsHolderSpace;

namespace DbConnectorSpace
{
    class DbConnector
    {
        
        public string Host{get;set;}
        public string User{get;set;}
        public string DBname{get;set;}
        public string Password{get;set;}
        public string Port{get;set;}
        public List<string> migrationsId;
        public string connString;

        MigrationsHolder migrationsHolder;

        public DbConnector(){} 
        public DbConnector(string configPath, string migrationPath) {
            migrationsHolder = new MigrationsHolder(migrationPath);
            migrationsId = new List<string>();
            initFromConfig(configPath);
            connString = 
                String.Format(
                    "Server={0};Username={1};Database={2};Port={3};Password={4};",
                    Host,
                    User,
                    DBname,
                    Port,
                    Password);
            getMigrations();
        }
        private void initFromConfig(string path) {
            if (File.Exists(path))
            {
                var config_json = File.ReadAllText(path);
                Console.WriteLine(config_json);
                DbConnector db = JsonSerializer.Deserialize<DbConnector>(config_json);
                this.Host = db.Host;
                this.Password = db.Password;
                this.DBname = db.DBname;
                this.Port = db.Port;
                this.User = db.User;
            }
        }

        private void getMigrations() {
            using (var conn = new NpgsqlConnection(connString))
            {
                conn.Open();
                using (var command = new NpgsqlCommand("SELECT * FROM migrations_history", conn))
                {
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        migrationsId.Add(reader.GetString(0));
                    }
                }
            } 
        }   

        private List<string> checkOnNewMigrations(List<string> allMigrations) {
            var newMigrations = new List<string>();
            foreach (var localMigration in allMigrations)
            {
                bool flag = true;
                foreach (var dbMigratin in migrationsId)
                {
                    if (dbMigratin == localMigration) {
                        flag =false;
                        break;
                    }
                }
                if (flag) newMigrations.Add(localMigration);
            }
            return newMigrations;
        }

        public void MakeMigrations() {
            var filesNeedToMigrate = checkOnNewMigrations(migrationsHolder.migrationsId);
            var commands = migrationsHolder.GetCommandsMigration(filesNeedToMigrate);
            
            //Проверить что собралось пушиться в бд

            // foreach (var file in filesNeedToMigrate)
            // {
            //     Console.WriteLine(file);
            // }
            // foreach (var command in commands)
            // {
            //     Console.WriteLine(command);
            // }
            
            using (var conn = new NpgsqlConnection(connString))
            {
                Console.Out.WriteLine("Opening connection");
                conn.Open();
                for (int i = 0; i < commands.Count; i++)
                {              
                    using (var command = new NpgsqlCommand("INSERT INTO migrations_history (id, command) VALUES (@i, @c)", conn))
                    {
                        command.Parameters.AddWithValue("i", filesNeedToMigrate[i]);
                        command.Parameters.AddWithValue("c", commands[i]);
                        command.ExecuteNonQuery();
                    }
                    using (var command = new NpgsqlCommand(commands[i], conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
        }
        
    }
}
