﻿using System;
using MigrationsHolderSpace;
using DbConnectorSpace;

namespace MigrationService
{
    class Program
    {
        static void Main(string[] args)
        {      
            string configPath = "/home/burunduck/Desktop/work/Project/MigrationService/db.json";
            string migrationPath = "/home/burunduck/Desktop/work/Project/MigrationService/migrations";
            var db = new DbConnector(configPath, migrationPath);
            db.MakeMigrations();
        }
    }
}
