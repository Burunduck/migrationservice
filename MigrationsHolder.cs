using System;
using System.Collections.Generic;
using System.IO;

namespace MigrationsHolderSpace
{
    class MigrationsHolder
    {
        private string dirPath{get;}
        public List<string> migrationsId;
        public MigrationsHolder(string dirPath) {
            migrationsId = new List<string>();
            if(Directory.Exists(dirPath)) {
                this.dirPath = dirPath;
                DirectoryInfo dir = new DirectoryInfo(dirPath);
                foreach (var file in dir.GetFiles())
                {
                    migrationsId.Add(file.Name);
                }
            }
        }
        public List<string> GetCommandsMigration(List<string> needsMigrationsId) { //только те айдишники, что нужны
            var commands = new List<string>();
            foreach (var fileName in needsMigrationsId)
            {
                commands.Add(File.ReadAllText(dirPath+"/"+fileName));
            }
            return commands;
        }
    }
}
